package com.techuniversity.facs.controllers;

import com.techuniversity.facs.domain.FacturaModel;
import com.techuniversity.facs.repositories.FacturaRepository;
import com.techuniversity.facs.repositories.ProductoRepository;
import com.techuniversity.facs.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/facturas")
public class FacturaController {

    @Autowired
    FacturaRepository facturaRepository;

    @GetMapping("/facturas")
    public Iterable<FacturaModel> getAll() {
        return facturaRepository.findAll();
    }

    @GetMapping("/facturas")
    public Iterable<FacturaModel> getAll(@RequestParam(defaultValue = "0") double hasta) {
        if (hasta == 0) {
            return facturaRepository.findAll();
        } else {
            return facturaRepository.findByImporte(hasta);
        }
    }

    @GetMapping("/facturas/fecha")
    public Iterable<FacturaModel> getByFecha(@RequestParam(defaultValue = "") String fecha) {
        if (fecha.equals("")) {
            return facturaRepository.findAll();
        } else {
            return facturaRepository.findByFecha(fecha);
        }
    }

    @Autowired
    ProductoService productoService;

    @PostMapping("/facturas")
    public String crearFactura(@RequestParam String cliente, @RequestParam int idProducto) {
        try {
            productoService.crearFactura(cliente, idProducto);
            return "OK";
        } catch (Exception e) {
            return "KO";
        }
    }


}

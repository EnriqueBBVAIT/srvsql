package com.techuniversity.facs.services;

import com.techuniversity.facs.domain.FacturaModel;
import com.techuniversity.facs.domain.ProductoModel;
import com.techuniversity.facs.repositories.FacturaRepository;
import com.techuniversity.facs.repositories.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    FacturaRepository facturaRepository;
    @Autowired
    ProductoRepository productoRepository;

    @Transactional(rollbackOn = {Exception.class})
    public void crearFactura(String cliente, int idProducto) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = dateFormat.format(new Date());

        Optional<ProductoModel> productoModel = productoRepository.findById(idProducto);
        if (productoModel.isPresent()) {
            ProductoModel producto = productoModel.get();
            FacturaModel factura = new FacturaModel();
            factura.setCliente(cliente);
            factura.setFecha(fecha);
            factura.setId_producto(idProducto);
            factura.setImporte(producto.getPrecio());
            facturaRepository.save(factura);
            if ((producto.getStock() - 1) < 0) throw new Exception("Stock <0");
        }
    }

}


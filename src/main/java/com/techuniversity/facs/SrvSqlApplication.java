package com.techuniversity.facs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrvSqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvSqlApplication.class, args);
    }

}

package com.techuniversity.facs.repositories;

import com.techuniversity.facs.domain.ProductoModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductoRepository extends CrudRepository<ProductoModel, ProductoRepository> {

    Optional<ProductoModel> findById(int id);

}

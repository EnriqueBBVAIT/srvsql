package com.techuniversity.facs.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
//@NamedQuery(name = "FacturaModel.findByImporte", query = "from com.techuniversity.facs.domain.FacturaModel where importe > ?1"")
@NamedQuery(name = "FacturaModel.findByImporte", query = "")
@NamedNativeQuery(name = "FacturaModel", query = "select * from facturas a where date(a.fecha) <= date(?1)", resultClass = FacturaModel.class)
@Entity(name = "facturas")
public class FacturaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String fecha;
    private String cliente;
    private int id_producto;
    private double importe;

}
